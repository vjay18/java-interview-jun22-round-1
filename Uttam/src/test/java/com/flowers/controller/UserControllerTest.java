package com.flowers.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.flowers.controller.UserController;
import com.flowers.dto.UniqueUserIdDTO;
import com.flowers.dto.UserDTO;
import com.flowers.util.JsonUtil;

@SpringBootTest
@TestMethodOrder(MethodOrderer.MethodName.class)
public class UserControllerTest {
	
	@Autowired
	private UserController userController;
	
	
	@Test
	public void uniqueUserIdCount_thenStatus200() {
		ResponseEntity<?> responseEntity=userController.uniqueUserIdCount();
		UniqueUserIdDTO uniUser=(UniqueUserIdDTO) responseEntity.getBody();
		assertEquals(10,uniUser.getUniqueUserIdCount());
		assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
	}
	
	@Test
	public void updateUser_thenStatus200() {
		UserDTO userDTO=new UserDTO();
		userDTO.setBody("1800Flowers");
		userDTO.setTitle("1800Flowers");
		ResponseEntity<?> responseEntity=userController.updateUser(Long.valueOf(4), userDTO);
		UserDTO userResponse=(UserDTO) responseEntity.getBody();
		assertEquals(userResponse.getBody(), "1800Flowers");
		assertEquals(userResponse.getTitle(), "1800Flowers");
		assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
	}
	
	@Test
	public void getAllUser_thenStatus200() {
		ResponseEntity<?> responseEntity=userController.getAllUsers();
		assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
	}

	@Test
	public void testWhenUserObjectIsBlank() {
		UserController userController=Mockito.mock(UserController.class);
		Mockito.when(userController.getAllUsers()).thenReturn(null);
		ResponseEntity<?> responseEntity = userController.getAllUsers();
		assertNull(responseEntity);
	}
	 
}
