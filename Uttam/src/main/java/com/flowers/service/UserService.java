package com.flowers.service;

import java.util.List;

import com.flowers.dto.UniqueUserIdDTO;
import com.flowers.dto.UserDTO;

public interface UserService {

	/**
	 * This API would count unique user by user id and tally with id 
	 * @param userId
	 * @return
	 */
	public UniqueUserIdDTO uniqueUserIdCount();
	
	/**
	 * This API would update the user title and body at specified index and default to 4th index 
	 * @param id
	 * @param userDTO
	 * @return
	 */
	public UserDTO updateUserByIndex(Long index,UserDTO userDTO);

	/**
	 * This API will return all users
	 * @return
	 */
	public List<UserDTO> getAllUsers();
}

