package com.flowers.serviceImpl;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.flowers.dto.UniqueUserIdDTO;
import com.flowers.dto.UserDTO;
import com.flowers.exception.UserException;
import com.flowers.service.UserService;
import com.flowers.util.JsonUtil;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${user.api.url}")
	private String userApiUrl;
	
	public static List<UserDTO> userDtoList=null;
	
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public List<UserDTO> addUsers() throws StreamReadException, DatabindException, IOException {
		String json = restTemplate.getForObject(userApiUrl, String.class);
		userDtoList=(List<UserDTO>) JsonUtil.convertJsonToList(json, UserDTO.class);
		return userDtoList;
	}

	@Override
	public UniqueUserIdDTO uniqueUserIdCount() {
		UniqueUserIdDTO uniqueUserIdDTO=new UniqueUserIdDTO();
		Map<Long, Long> countForId = userDtoList.stream().distinct()
		        .collect(Collectors.groupingBy(UserDTO::getUserId, Collectors.counting()));
		long uniqeUserIdCount = countForId.values().stream().reduce(0l,Long::sum);
		uniqueUserIdDTO.setUniqueUserIdCount(uniqeUserIdCount);
		return uniqueUserIdDTO;
	}

	@Override
	public UserDTO updateUserByIndex(Long index, UserDTO userDTO) {
		if(userDtoList.isEmpty())
			throw new UserException("No user found", HttpStatus.NOT_FOUND);
		userDtoList.get(index.intValue()).setBody(userDTO.getBody());
		userDtoList.get(index.intValue()).setTitle(userDTO.getTitle());
		return userDtoList.get(index.intValue());
	}

	@Override
	public List<UserDTO> getAllUsers() {
		return userDtoList;
	}
	
}
