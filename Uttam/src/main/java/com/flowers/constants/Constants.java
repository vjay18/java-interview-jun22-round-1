package com.flowers.constants;

public class Constants {
	
	private Constants() {
		throw new IllegalStateException("Constants class");
	}
	
	public static final String UNIQUE_USER_ID_COUNT="getUniqueUserIdCount";
	
	public static final String UPDATE_USER_DATA_BY_INDEX="updateUserByIndex/index/{index}";
	
	public static final String GET_ALL_USER="getAllUsers";

}
