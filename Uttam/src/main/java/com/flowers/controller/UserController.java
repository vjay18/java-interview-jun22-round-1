package com.flowers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.flowers.constants.Constants;
import com.flowers.dto.UserDTO;
import com.flowers.exception.UserException;
import com.flowers.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	/**
	 * This API would count the number of unique ids by user id and return the response
	 * @param userId
	 * @return
	 */
	
	@GetMapping(path = Constants.UNIQUE_USER_ID_COUNT)
	public ResponseEntity<?> uniqueUserIdCount() {
		return ResponseEntity.ok(userService.uniqueUserIdCount());
	}

	
	/**
	 * This API would update the title and body at provided index
	 * @param index
	 * @param userDTO
	 * @return
	 */
	@PutMapping(path = Constants.UPDATE_USER_DATA_BY_INDEX)
	public ResponseEntity<?> updateUser(@PathVariable(value = "index", required = false) Long index, @RequestBody UserDTO userDTO) {
		return ResponseEntity.ok(userService.updateUserByIndex(index, userDTO));
	}
	
	
	/**
	 * This API would return all users
	 * @param index
	 * @param userDTO
	 * @return
	 */
	@GetMapping(path = Constants.GET_ALL_USER)
	public ResponseEntity<?> getAllUsers() {
		return ResponseEntity.ok(userService.getAllUsers());
	}

}
