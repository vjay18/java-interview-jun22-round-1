package com.flowers.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class UserExceptionAdvisor extends ResponseEntityExceptionHandler{

	@ExceptionHandler({UserException.class})
	ResponseEntity<?> handleAppException(UserException error){
		return ResponseEntity.status(error.getStatus()).body(error);
	}
	
}
