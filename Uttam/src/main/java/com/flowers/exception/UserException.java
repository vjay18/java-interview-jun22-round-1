package com.flowers.exception;

import org.springframework.http.HttpStatus;

public class UserException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7549060278235233352L;

	
	private String message;
	private HttpStatus status;
	
	public UserException(String message, HttpStatus status) {
		super();
		this.message = message;
		this.status = status;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
}
