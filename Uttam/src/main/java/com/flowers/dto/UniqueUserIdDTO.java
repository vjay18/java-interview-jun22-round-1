package com.flowers.dto;

public class UniqueUserIdDTO {
	
	Long uniqueUserIdCount;

	/**
	 * @return the uniqueUserIdCount
	 */
	public Long getUniqueUserIdCount() {
		return uniqueUserIdCount;
	}

	/**
	 * @param uniqueUserIdCount the uniqueUserIdCount to set
	 */
	public void setUniqueUserIdCount(Long uniqueUserIdCount) {
		this.uniqueUserIdCount = uniqueUserIdCount;
	}

	@Override
	public String toString() {
		return "UniqueUserIdDTO [uniqueUserIdCount=" + uniqueUserIdCount + "]";
	}
	
}
