package com.flowers.util;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {
	
	private  static ObjectMapper objectMapper;
	static {
		objectMapper=new ObjectMapper();
	}
	
	public static List<?> convertJsonToList(String json,Class<?> classes) throws StreamReadException, DatabindException, IOException{
		return objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, classes));
	}
	
}
