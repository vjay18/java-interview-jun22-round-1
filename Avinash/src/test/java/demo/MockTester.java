package demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo.controller.DemoController;
import com.example.demo.dto.FeedDTO;
import com.example.demo.service.FeedService;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = MockTester.class)
public class MockTester {

	@Mock
	private FeedService feedService;

	@InjectMocks
	private DemoController demoCtrl;

	@Test
	@Order(1)
	void testGetFeedsCount() {
		long expectedCount = 10;
		when(feedService.countFeeds()).thenReturn(expectedCount);
		ResponseEntity<Object> responseEntity = demoCtrl.getFeedCount();
		assertEquals(HttpStatus.FOUND, responseEntity.getStatusCode());
	}

	@Test
	@Order(2)
	void testGetUniqueFeeds() {
		List<FeedDTO> list = new ArrayList<>();
		list.add(new FeedDTO(1, 1, "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
				"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto"));
		list.add(new FeedDTO(2, 2, "qui est esse",
				"est rerum tempore vitae\\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\\nqui aperiam non debitis possimus qui neque nisi nulla"));
		list.add(new FeedDTO(3, 3, "et ea vero quia laudantium autem",
				"delectus reiciendis molestiae occaecati non minima eveniet qui voluptatibus\\naccusamus in eum beatae sit\\nvel qui neque voluptates ut commodi qui incidunt\\nut animi commodi"));
		when(feedService.getUniqueFeeds()).thenReturn(list);
		ResponseEntity<Object> responseEntity = demoCtrl.getUniqueFeeds();
		assertEquals(HttpStatus.FOUND, responseEntity.getStatusCode());
	}

	@Test
	@Order(3)
	void testModifyFeeds() {
		FeedDTO feedUpdated=new FeedDTO(1, 4,"1800Flowers","1800Flowers");
		when(feedService.getModifiedFeed()).thenReturn(feedUpdated);
		ResponseEntity<Object> responseEntity = demoCtrl.modifyFeeds();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
}