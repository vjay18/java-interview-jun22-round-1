package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.FeedDTO;

public interface FeedService {
	List<FeedDTO> getUniqueFeeds();
	Long countFeeds();
	FeedDTO getModifiedFeed();
}
