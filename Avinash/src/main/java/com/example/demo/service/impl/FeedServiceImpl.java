package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.config.HttpConnect;
import com.example.demo.dto.FeedDTO;
import com.example.demo.service.FeedService;
import com.example.demo.util.Utilities;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FeedServiceImpl implements FeedService {

	@Value("${feeds.endpoint}")
	private String feedsEndpoint;

	@Value("${titleToUpdate}")
	private String titleNameToUpdate;
	
	@Value("${bodyToUpdate}")
	private String bodyToUpdate;
	
	@Autowired
	private HttpConnect httpConnect;

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	// Service method to return unique feeds after fetching feeds from endpoint
	@SuppressWarnings("deprecation")
	@Override
	public List<FeedDTO> getUniqueFeeds() {
		log.info("Inside getUniqueFeeds method for fetching unique feeds");
		try {
			String responseString = httpConnect.send(feedsEndpoint);
			if (!StringUtils.isEmpty(responseString)) {
				log.info("responseString before conversion is {}",responseString);
				// Parsing string to JSON of type List<FeedDTO>
				List<FeedDTO> feedsList = Utilities.getFeedListFromString(responseString);
				// Java 8 feature to filter unique user id's and return in new list using collectors
				return feedsList.stream().filter(e -> null != e).filter(distinctByKey(FeedDTO::getUserId))
						.collect(Collectors.toList());

			} else {
				log.info("Null or Blank response received from endpoint {} for processing getUniqueFeeds",feedsEndpoint);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), new Exception(e));
		}
		return new ArrayList<>();
	}

	// Service method to return feeds count after fetching feeds from endpoint
	@SuppressWarnings("deprecation")
	@Override
	public Long countFeeds() {
		try {
			String responseString = httpConnect.send(feedsEndpoint);
			if (!StringUtils.isEmpty(responseString)) {
				// Parsing string to JSON of type List<FeedDTO>
				List<FeedDTO> feedsList = Utilities.getFeedListFromString(responseString);
				// Java 8 feature to get count of the list
				return feedsList.stream().count();
			} else {
				log.info("Null or Blank response received from endpoint {} for processing countFeeds ",feedsEndpoint);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), new Exception(e));
		}
		return 0l;
	}

	// Service method to return feed after modifying feed after fetching feeds from endpoint
	@SuppressWarnings("deprecation")
	@Override
	public FeedDTO getModifiedFeed() {
		try {
			String responseString = httpConnect.send(feedsEndpoint);
			if (!StringUtils.isEmpty(responseString)) {
				// Parsing string to JSON of type List<FeedDTO>
				List<FeedDTO> feedsList = Utilities.getFeedListFromString(responseString);
				// Java 8 feature to filter stream of type list to modify the element having id 4
				// after getting the element its tittle and body is replaced using map function of stream and returning the first match
				Optional<FeedDTO> feedAfterModify = feedsList.stream().filter(e -> null != e && e.getId() == 4)
						.map(e -> {
							e.setTitle(titleNameToUpdate);
							e.setBody(bodyToUpdate);
							return e;
						}).findFirst();
				return feedAfterModify.orElse(new FeedDTO());
			} else {
				log.info("Null or Blank response received from endpoint {} for processing getModifiedFeed",feedsEndpoint);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), new Exception(e));
		}
		return null;
	}
}