package com.example.demo.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.dto.FeedDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Utilities {

	public static List<FeedDTO> getFeedListFromString(String receivedResponse){
		log.info("Inside getFeedListFromString with {} to convert in List<FeedDTO>",receivedResponse);
		try {
			Gson gson = new Gson();
			Type type1 = new TypeToken<List<FeedDTO>>() {
			}.getType();
			return gson.fromJson(receivedResponse, type1);
		}catch(Exception exp) {
			log.error(exp.getMessage(),new Exception(exp));
			return new ArrayList<>();
		}
	}
}