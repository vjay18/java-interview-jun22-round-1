package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.FeedService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "${feeds.ctrl.name}")
@Slf4j
public class DemoController {

	@Autowired
	FeedService feedsService;
	
   //	API for fetching feed count
	@GetMapping(path = "${feeds.api.count.name}")
	public ResponseEntity<Object> getFeedCount() {
		log.debug("Inside countFeeds REST API for fetching feeds count.");
		try {
			return new ResponseEntity<Object>(feedsService.countFeeds(), HttpStatus.FOUND);
		} catch (Exception e) {
			log.error(e.getMessage(),new Exception(e));
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
   //	API for fetching unique feeds
	@GetMapping(path = "${feeds.api.unique.feeds.name}")
	public ResponseEntity<Object> getUniqueFeeds() {
		log.debug("Inside getUniqueFeeds REST API for fetching unique feeds");
		try {
			return new ResponseEntity<Object>(feedsService.getUniqueFeeds(), HttpStatus.FOUND);
		} catch (Exception e) {
			log.error(e.getMessage(),new Exception(e));
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	// API to modify title and body
	@GetMapping(path = "${feeds.api.modify.feeds.name}")
	public ResponseEntity<Object> modifyFeeds() {
		log.debug("Inside modifyFeed REST API for modifying feeds");
		try {
			return new ResponseEntity<Object>(feedsService.getModifiedFeed(), HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(),new Exception(e));
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}