package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HttpConnect {

	@Autowired
	private RestTemplate restTemplate;

	public String send(final String reqUrl) {
		log.debug("Inside send method with URL: {}, reqParams: {} and txn id:{}", reqUrl);
		String response = "";
		try {
			long reqTime = System.currentTimeMillis();
			log.debug("Before sending request req time is as {}", reqTime);
			ResponseEntity<String> httpResponse = restTemplate.getForEntity(reqUrl, String.class);
			long respTime = System.currentTimeMillis();
			log.debug("After receiving response resp time is :{}", respTime);
			if (!StringUtils.isEmpty(httpResponse) && HttpStatus.OK.equals(httpResponse.getStatusCode())) {
				response = httpResponse.getBody();
			}
			log.debug("Response:{} received {} ", response);
		} catch (Exception e) {
			log.error(e.getMessage(), new Exception(e));
		}
		log.debug("send ended with return value as {}", response);
		return response;
	}

}
